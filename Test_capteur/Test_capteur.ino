#include "DHT.h"
#define DHTPIN 2
#define DHTTYPE DHT11   
#define PIR 3
DHT dht(DHTPIN, DHTTYPE);

int smokeA5 = A5;
int sensorThres = 400;

void setup() {
  Serial.begin(115200);
  dht.begin();
  pinMode(smokeA5, INPUT);
  pinMode(PIR, INPUT);
}

void loop() {
  //////////////////////////////////////////////temperature
  float humidity = dht.readHumidity();
  float t = dht.readTemperature();
  float celsius = dht.computeHeatIndex(t, h, false);
  Serial.print(F("Humidity: "));
  Serial.print(humidity);
  Serial.print(F("°F  Heat index: "));
  Serial.print(celsius);
  Serial.print(F("°C  /  "));
  //////////////////////////////////////////////C02
  int CO2censor = analogRead(smokeA5);
  Serial.print("C02: ");
  Serial.print(CO2censor);
  Serial.print("ppm / ");
  //////////////////////////////////////////////Mouvement
  int motion = digitalRead(PIR);
   if (motion == HIGH) {
      Serial.println("Mouvement détecté");
   }
   else{
    Serial.println("");
   }
}
